package katarzyna.mobileapp7;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Toast;

public class Activity2 extends AppCompatActivity
{

    RatingBar ratingBar;
    ImageView imageViewLogo;
    ProgressBar progressBar;
    LayerDrawable stars;
    ActionMode actionMode;

    boolean rotation180;
    boolean rotation45;
    boolean rotation90;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        rotation180=false;
        rotation45=false;
        rotation90=false;

        ratingBar=(RatingBar) findViewById(R.id.ratingBar); //menu zwykle
        imageViewLogo=(ImageView)findViewById(R.id.imageViewLogo); //menu kontekstowe z checkbox
        progressBar=(ProgressBar)findViewById(R.id.progressBar); //tryb kontekstowy akcji

        stars= (LayerDrawable) ratingBar.getProgressDrawable();
        registerForContextMenu(imageViewLogo);

        progressBar.setOnLongClickListener( new View.OnLongClickListener()
        {
            public boolean onLongClick(View view)
            {
                if(actionMode!=null)
                {
                    return false;
                }
                actionMode = startActionMode(mActionModeCallback);
                view.setSelected(true);
                return true;
            }
         });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        MenuItem option1 = menu.add(0,1,1,getResources().getString(R.string.firstMenuOption1));
        MenuItem option2 = menu.add(0,2,2,getResources().getString(R.string.firstMenuOption2));
        MenuItem option3 = menu.add(0,3,3,getResources().getString(R.string.firstMenuOption3));
        option1.setIcon(R.drawable.ico1);
        option2.setIcon(R.drawable.ico2);
        option3.setIcon(R.drawable.ico3);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case 1:
                ratingBar.setRating(5);
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                break;

            case 2:
                ratingBar.setRating(7);
                stars.getDrawable(2).setColorFilter(Color.rgb(204,0,204), PorterDuff.Mode.SRC_ATOP);
                break;

            case 3:
                ratingBar.setRating(3);
                stars.getDrawable(2).setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_ATOP);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();

        if(v.getId() == imageViewLogo.getId())
        {
            inflater.inflate(R.menu.checkable_menu,menu);
        }
        if(rotation180)
        {
            menu.findItem(R.id.buttonRotation1).setChecked(true);
            //menu.findItem(R.id.buttonRotation2).setChecked(false);
            //menu.findItem(R.id.buttonRotation3).setChecked(false);
        }
        if(rotation45)
        {
            //menu.findItem(R.id.buttonRotation1).setChecked(false);
            menu.findItem(R.id.buttonRotation2).setChecked(true);
            //menu.findItem(R.id.buttonRotation3).setChecked(false);
        }
        if(rotation90)
        {
            //menu.findItem(R.id.buttonRotation1).setChecked(false);
            //menu.findItem(R.id.buttonRotation2).setChecked(false);
            menu.findItem(R.id.buttonRotation3).setChecked(true);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.buttonRotation1:

                if (item.isChecked()) item.setChecked(true);
                if (rotation180)
                {
                    rotation180 = false;
                    imageViewLogo.setRotation(0);
                }
                else
                {
                    rotation180=true;
                    imageViewLogo.setRotation(180);
                }
                break;

            case R.id.buttonRotation2:

                if (item.isChecked()) item.setChecked(true);
                if (rotation45)
                {
                    rotation45 = false;
                    imageViewLogo.setRotation(0);
                }
                else
                {
                    rotation45=true;
                    imageViewLogo.setRotation(45);
                }
                break;

            case R.id.buttonRotation3:

                if (item.isChecked()) item.setChecked(true);
                if (rotation90)
                {
                    rotation90 = false;
                    imageViewLogo.setRotation(0);
                }
                else
                {
                    rotation90=true;
                    imageViewLogo.setRotation(90);
                }
                break;
        }

        return super.onContextItemSelected(item);
    }



    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback()
    {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu)
        {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_action_menu,menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu)
        {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item)
        {
            switch (item.getItemId())
            {
                case R.id.typeRed:
                    progressBar.getIndeterminateDrawable().setColorFilter(Color.RED, android.graphics.PorterDuff.Mode.MULTIPLY);
                    progressBar.setMax(25);
                    mode.finish();
                    break;

                case R.id.typeYellow:
                    progressBar.getIndeterminateDrawable().setColorFilter(Color.YELLOW, android.graphics.PorterDuff.Mode.MULTIPLY);
                    progressBar.setMax(50);
                    mode.finish();
                    break;

                case R.id.typeGreen:
                    progressBar.getIndeterminateDrawable().setColorFilter(Color.GREEN, android.graphics.PorterDuff.Mode.MULTIPLY);
                    progressBar.setMax(75);
                    mode.finish();
                    break;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode)
        {
            actionMode = null;
        }
    };

}
