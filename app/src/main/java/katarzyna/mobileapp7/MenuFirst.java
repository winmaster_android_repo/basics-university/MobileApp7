package katarzyna.mobileapp7;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MenuFirst extends AppCompatActivity
{
    EditText editTextName;
    EditText editTextSurname;
    TextView title1;
    TextView title2;
    Random rand;
    View currentView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_first);

        rand=new Random();
        editTextName=(EditText)findViewById(R.id.editTextName);
        editTextSurname=(EditText)findViewById(R.id.editTextSurname);
        currentView=(View)findViewById(R.id.activity_menu_first);
        title1= (TextView)findViewById(R.id.textViewTitleFirst);
        title2= (TextView)findViewById(R.id.textViewTitleSecond);

        registerForContextMenu(editTextName);
        registerForContextMenu(editTextSurname);
        registerForContextMenu(title1);
        registerForContextMenu(title2);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();

        if(v.getId() == editTextName.getId())
        {
            inflater.inflate(R.menu.context_menu_first_texts,menu);
            menu.setHeaderTitle(getResources().getString(R.string.contextMenuFirstTitle2));
            menu.setHeaderIcon(R.drawable.ico1);
        }
        if(v.getId() == editTextSurname.getId())
        {
            inflater.inflate(R.menu.context_menu_first_texts,menu);
            menu.setHeaderTitle(getResources().getString(R.string.contextMenuFirstTitle2));
            menu.setHeaderIcon(R.drawable.ico1);
        }
        if(v.getId() == title1.getId())
        {
            inflater.inflate(R.menu.context_menu_first_titles,menu);
            menu.setHeaderTitle(getResources().getString(R.string.contextMenuFirstTitle1));
            menu.setHeaderIcon(R.drawable.ico2);
        }
        if(v.getId() == title2.getId())
        {
            inflater.inflate(R.menu.context_menu_first_titles,menu);
            menu.setHeaderTitle(getResources().getString(R.string.contextMenuFirstTitle1));
            menu.setHeaderIcon(R.drawable.ico2);
        }
    }

    public boolean onContextItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {

            case R.id.ctxMenuOption1:
                title1.setBackgroundColor(Color.RED);
                title2.setBackgroundColor(Color.RED);
                break;

            case R.id.ctxMenuOption2:
                title1.setBackgroundColor(Color.BLUE);
                title2.setBackgroundColor(Color.BLUE);
                break;

            case R.id.ctxMenuOption3:
                title1.setBackgroundColor(Color.GREEN);
                title2.setBackgroundColor(Color.GREEN);
                break;

            case R.id.ctxMenuChoice1:
                editTextSurname.setText("Kowalski");
                editTextName.setText("Jan");
                break;

            case R.id.ctxMenuChoice2:
                editTextSurname.setText("Nowak");
                editTextName.setText("Anna");
                break;

            case R.id.ctxMenuChoice3:
                editTextSurname.setText("Kwiatkowski");
                editTextName.setText("Zenon");
                break;
        }

        return super.onContextItemSelected(item);
    }


    public boolean onCreateOptionsMenu(Menu first_menu)
    {
        super.onCreateOptionsMenu(first_menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_first, first_menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.option1:  //zmiana rozmiaru czczcionki
            {
                int size=rand.nextInt(40)+10;
                editTextName.setTextSize(size);
                editTextSurname.setTextSize(size);
                return true;
            }
            case R.id.option2: //zmiana koloru czcionki
            {
                int r=rand.nextInt(255);
                int g=rand.nextInt(255);
                int b=rand.nextInt(255);
                editTextName.setTextColor(Color.rgb(r,g,b));
                editTextSurname.setTextColor(Color.rgb(r,g,b));
                return true;
            }
            case R.id.option3: //zmienia kolor tla
            {
                int r=rand.nextInt(255);
                int g=rand.nextInt(255);
                int b=rand.nextInt(255);
                currentView.setBackgroundColor(Color.rgb(r,g,b));
                return true;
            }
            case R.id.option4: //czysci pole tekstowe
            {
                editTextName.setText("");
                editTextSurname.setText("");
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void goPage1(View view)
    {
        Intent intent = new Intent(this,MenuSecond.class);
        startActivity(intent);
    }

    public void goPage2(View view)
    {
        Intent intent = new Intent(this,Activity2.class);
        startActivity(intent);
    }
}
