package katarzyna.mobileapp7;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MenuSecond extends AppCompatActivity
{
    ImageView photoLeft;
    ImageView photoRight;
    ImageView photoObject;
    TextView title1;
    TextView title2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_second);

        photoLeft=(ImageView)findViewById(R.id.imageViewPhotoLeft);
        photoRight=(ImageView)findViewById(R.id.imageViewPhotoRight);
        photoObject=(ImageView)findViewById(R.id.imageViewObject);
        title1=(TextView)findViewById(R.id.textViewTitle1);
        title2=(TextView)findViewById(R.id.textViewTitle2);
    }

    public boolean onCreateOptionsMenu(Menu second_menu)
    {
        super.onCreateOptionsMenu(second_menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_second, second_menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.choice1:
            {
                title1.setText("Jeszcze nie naprawiono...");
                title2.setText("...Smutny...");
                photoLeft.setRotation(45);
                photoRight.setRotation(315);
                photoObject.setImageResource(R.drawable.sad_pikatchu);
                photoObject.setColorFilter(Color.RED, PorterDuff.Mode.LIGHTEN);
                return true;
            }
            case R.id.choice2:
            {
                title1.setText("Próbuj dalej!");
                title2.setText("...Smutniejszy...");
                photoLeft.setRotation(180);
                photoRight.setRotation(90);
                photoObject.setImageResource(R.drawable.sad_pikatchu);
                photoObject.setColorFilter(Color.BLUE, PorterDuff.Mode.ADD);
                return true;
            }
            case R.id.choice3:
            {
                title1.setText("Brawo!");
                title2.setText("...Wesoły...");
                photoLeft.setRotation(0);
                photoRight.setRotation(0);
                photoObject.setColorFilter(null);
                photoObject.setImageResource(R.drawable.happy_pikatchu);
                return true;
            }
            case R.id.choice4:
            {
                title1.setBackgroundColor(Color.RED);
                title2.setBackgroundColor(Color.RED);
                title1.setTextColor(Color.WHITE);
                title2.setTextColor(Color.WHITE);
                return true;
            }
            case R.id.choice5:
            {
                title1.setBackgroundColor(Color.BLUE);
                title2.setBackgroundColor(Color.BLUE);
                title1.setTextColor(Color.WHITE);
                title2.setTextColor(Color.WHITE);
                return true;
            }
            case R.id.choice6:
            {
                title1.setBackgroundColor(Color.GREEN);
                title2.setBackgroundColor(Color.GREEN);
                title1.setTextColor(Color.WHITE);
                title2.setTextColor(Color.WHITE);
                return true;
            }
            case R.id.choice7:
            {
                title1.setBackgroundColor(Color.WHITE);
                title2.setBackgroundColor(Color.WHITE);
                title1.setTextColor(Color.BLACK);
                title2.setTextColor(Color.BLACK);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
